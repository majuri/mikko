# Mikko Majuri
## Kehittäjä

> [majuri.mikko@gmail.com](majuri.mikko@gmail.com)

> +358 44 555 9229

> [Linkedin](http://www.linkedin.com/pub/mikko-majuri/3/1b0/a4)

> [Bitbucket](https://bitbucket.org/majuri)

> [CV PDF -linkki](http://dl.dropbox.com/u/82579239/mikko_majuri.pdf)


------

## Taidot
__Web-kehitys__
	: 15 vuotta webisivuja, viimeiset vuodet rakkauden lisäksi myös rahasta.

__Agiilit toimintametodit__
	: Vuosien kokemus toimivista ja toimimattomista toimintametodeista.

__Mukautuvuus__
	: 48h hackathonista, Rick Astleyn laulamiseen 800 ihmiselle – ei ole tilannetta, johon en sopeutuisi.

------

## Tekniset

1. Javascript
1. Coffeescript
1. Node.js
1. Backbone.js
1. Bacon.js
1. jQuery
1. MongoDb
1. Java
1. HTML
1. CSS
1. PHP
1. XML
1. Git

------

## Työkokemus
Houston Inc.
: *Senior Consultant/Coach/Community Gardener*
 __(Tammikuu 2014 – )__
 Toimin Suomen suurimman verkkokaupan (veikkaus.fi) frontend-kehittäjänä tehden Bacon.js:sää, Coffeescriptiä ja jQuerya. Lisäksi agiilien toimintamenetelmien koulutuksien järjestämistä asiakasyrityksille ja Houston Inc. sisäisen osaamisen kehittämistä Community Gardenerin roolissa.

ChyronHego Finland
: *Kehittäjä/Lähetysgraafikko*
 __(Maaliskuu 2013 – Tammikuu 2014)__
 Lähetysgrafiikan näyttämiseen tarkoitettavan ohjelmiston, CGC:n, ohjelmointia. Vastuulla mm. Vaahteraliigan grafiikoiden valmistelu.
 
ShopTilt Ltd.
: *Kehittäjä* 
 __(Elokuu 2012 – Tammikuu 2013)__
 Johdin menestyksekkäästi kolmen kuukauden e-kauppa -migraation Hongkongissa. Zend-arkkitehtuuria PHP- ja XML-kielillä.

85 Media
: *Kehittäjä/Co-founder*
 __(Kesäkuu 2012 – )__
 Uudenlaisen sosiaalisen median palvelun, Breakitin, kehittäminen nollasta Appstoressa julkaistavaan kuntoon. Teknisen toteutuksen alullepaneva voima. Vastuussa serveripuolen kehittämisestä ja kehityksen hallinnasta. 

Canalplus/Mtv3 Media
: *Lähetysgraafikko*
 __(Elokuu 2010 – Syyskuu 2012)__
 Urheilulähetystän grafiikoiden valmistelua ja livelähetysten grafiikoiden lähetykseen ajamista. Mukana Suomen suurimmassa televisiohankkeessa (Jääkiekon MM-kotikisat 2012).

Nautics Oy
: *Kehittäjä*
 __(Kesäkuu 2010 – Joulukuu 2010)__
 Venelijöille tarkoitetun Sailmate-reittisuunnittelupalvelun kehittämistä start-upissa tiimin jäsenenä. Vastuulla käyttöliittymätoteutus.

FC Sovelto Oyj
: *Markkinointiassistentti*
 __(Maaliskuu 2009 – Kesäkuu 2010)__
 Suurten sähköpostimarkkinointikamppanioiden hallinta ja suorittaminen. Asiakasrekisterin ylläpitoa.

Helsingin opetusvirasto / Pakilan Yläaste
: *Opettajan sijainen*
 __(Syyskuu 2005 – Tammikuu 2010)__
 Lähes kaikkien yläasteaineiden sijaistamista. Mm. kahden kuukauden teknisen työn sijaisuus.

Muu työkokemus
: *Mm. Siivooja, metallityömies, varastoapulainen*
 __(1998 – 2004)__

-----

## Koulutus
Aalto-yliopisto
: *Tekniikan ylioppilas*
 __(2005 – )__ Informaatioverkostojen koulutusohjelmassa pääaineena Mediatekniikka informaatioverkostoissa.
